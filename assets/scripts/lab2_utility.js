
// code a utility function called createHTMLElem which you will use in page.js
/**
 * @function - createHTMLElem
 * @param {HTMLElement} parent 
 * @param {String} elemtagText - name of the tag for e.g., 'p', 'li' 
 * @param {String} id - attrib id
 * @param {String} name - attrib name
 * @param {String} value - attrib value
 * @param {String} text - attrib textContent)
 * @param {String} placeHolder - attrib placeholder
 * @param {String} type - attrib type
 * @param {String} class_  - attrib class (Q. why the underscore?)
 * 
 * create an element whose tag is elemtagText ans set any of the attributes if they are supplied
 * return the created element
 * @Note - Use a rest param (an array) for the attributes, like ...args
 */
//----------------------------------------