export default [
    {
        languageOptions: {
    
     ecmaVersion: "latest",
     sourceType: "script",
   },
 rules: {
         // copied from https://github.com/eslint/eslint/blob/main/packages/js/src/configs/eslint-recommended.js
         // so we don't have to install @eslint/js which doesn't work with a global install
         // eslint-recommended
         "constructor-super": "error",
         "for-direction": "error",
         "getter-return": "error",
         "no-async-promise-executor": "error",
         "no-case-declarations": "error",
         "no-class-assign": "error",
         "no-compare-neg-zero": "error",
         "no-cond-assign": "error",
         "no-const-assign": "error",
         "no-constant-binary-expression": "error",
         "no-constant-condition": "error",
         "no-control-regex": "error",
         "no-debugger": "error",
         "no-delete-var": "error",
         "no-dupe-args": "error",
         "no-dupe-class-members": "error",
         "no-dupe-else-if": "error",
         "no-dupe-keys": "error",
         "no-duplicate-case": "error",
         "no-empty": "error",
         "no-empty-character-class": "error",
         "no-empty-pattern": "error",
         "no-empty-static-block": "error",
         "no-ex-assign": "error",
         "no-extra-boolean-cast": "error",
         "no-fallthrough": "error",
         "no-func-assign": "error",
         "no-global-assign": "error",
         "no-import-assign": "error",
         "no-invalid-regexp": "error",
         "no-irregular-whitespace": "error",
         "no-loss-of-precision": "error",
         "no-misleading-character-class": "error",
         "no-new-native-nonconstructor": "error",
         "no-nonoctal-decimal-escape": "error",
         "no-obj-calls": "error",
         "no-octal": "error",
         "no-prototype-builtins": "error",
         "no-redeclare": "error",
         "no-regex-spaces": "error",
         "no-self-assign": "error",
         "no-setter-return": "error",
         "no-shadow-restricted-names": "error",
         "no-sparse-arrays": "error",
         "no-this-before-super": "error",
         "no-undef": "error",
         "no-unexpected-multiline": "error",
         "no-unreachable": "error",
         "no-unsafe-finally": "error",
         "no-unsafe-negation": "error",
         "no-unsafe-optional-chaining": "error",
         "no-unused-labels": "error",
         "no-unused-private-class-members": "error",
         "no-unused-vars": "error",
         "no-useless-backreference": "error",
         "no-useless-catch": "error",
         "no-useless-escape": "error",
         "no-with": "error",
         "require-yield": "error",
         "use-isnan": "error",
         "valid-typeof": "error",
         // custom rules
         "no-console": "off",
         "no-unused-vars": "warn",
         "no-undef": "off",
         "no-extra-parens": "warn",
         indent: ["error", 2],
         "block-scoped-var": "warn",
         curly: "warn",
         "default-case": "warn",
         "dot-location": ["warn", "object"],
         eqeqeq: "warn",
         "no-alert": "warn",
         "no-eq-null": "warn",
         "no-eval": "warn",
         "no-implicit-coercion": "warn",
         "no-lone-blocks": "error",
         "no-loop-func": "warn",
         "no-multi-str": "warn",
         "no-self-compare": "warn",
         strict: ["warn", "global"],
         "no-lonely-if": "warn",
         "array-bracket-spacing": ["warn", "never"],
         "array-bracket-newline": ["warn", "consistent"],
         camelcase: "error",
 
         "comma-spacing": ["warn", {
           before: false,
           after: true,
         }],
 
         "comma-style": ["warn", "last"],
         "brace-style": ["warn"],
         "max-len": ["warn", 100],
         "no-inline-comments": "error",
         "no-tabs": "error",
         "space-infix-ops": "warn",
         "space-unary-ops": "warn",
         quotes: "error",
       },
       ignores: ["eslint.config.mjs"]
    }
 ];
