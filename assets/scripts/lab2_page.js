// ----  given code   for toggling HTML5 buit-in form validation using attr novalidate

let novalidate = document.querySelectorAll('[name=novalidate]');
let novalidateNode= document.createAttribute('novalidate');
let form  =document.querySelector('form');
let sectionError = document.querySelector('#section-errors');

form.setAttributeNode(novalidateNode);
novalidate.forEach ((btn)=>btn.addEventListener('click', function(){
            if(btn.value==='on')
                form.setAttributeNode(novalidateNode);
            else
            form.removeAttributeNode(novalidateNode);
                
                        } ));


let petfield = document.querySelector('#pets-info');
let petsection = document.querySelector('#pets-section');
let select = document.querySelector('#num-pets');
let btn = document.querySelector('button');

function appendPet(amount)
{
    sectionError.textContent = "";
    petfield.innerHTML = '';

    if(amount === 0)
    {
        sectionError.textContent = "No pets";
        btn.style.display = "none";
    } else { btn.style.display = "block"; }

    for (let i = 0; i < amount; i++)
    {
        const mainSection = document.createElement('section');
        mainSection.setAttribute('class', 'form-fields-col');
        mainSection.setAttribute('sect-num', i);

        const nameLabel = document.createElement('label');
        nameLabel.textContent = 'Name:';

        const nameInput = document.createElement('input');
        nameInput.setAttribute('type', 'text');
        nameInput.setAttribute('name', 'petname');
        nameInput.setAttribute('pos', i);
        nameInput.setAttribute('required', '');

        nameInput.addEventListener("input", (e) => {
            let x = document.querySelectorAll('[name=petname]');
            for(let i = 0; i < x.length; i++)
            {
                if(nameInput.value === x[i].value && nameInput.getAttribute('pos') !== x[i].getAttribute('pos'))
                {
                    sectionError.textContent = "Duplicate names detected!";
                    btn.style.display = 'none';
                    return 0;
                }
                sectionError.textContent = "";
                btn.style.display = 'block';
            }
        });

        const ageLabel = document.createElement('label');
        ageLabel.textContent = 'Age:';

        const ageInput = document.createElement('input');
        ageInput.setAttribute('type', 'number');
        ageInput.setAttribute('name', 'petage');
        ageInput.setAttribute('min', '1');
        ageInput.setAttribute('max', '30');
        ageInput.setAttribute('required', '');

        const petSection = document.createElement('section');
        petSection.setAttribute('class', 'form-fields-col');

        

        let petSectionC = document.createElement('section');
        petSectionC.setAttribute('class', 'form-fields-row');
        petSectionC.setAttribute('id', 'cat-field');
        let petSectionD = document.createElement('section');
        petSectionD.setAttribute('class', 'form-fields-row');
        petSectionD.setAttribute('id', 'dog-field');
        let petSectionO = document.createElement('section');
        petSectionO.setAttribute('class', 'form-fields-row');
        petSectionO.setAttribute('id', 'other-field');

        let typeLabelO = document.createElement('label');
        typeLabelO.setAttribute('for', '');
        typeLabelO.textContent = 'Other';
        let typeInputO = document.createElement('input');
        typeInputO.setAttribute('type', 'radio');
        typeInputO.setAttribute('name', i);
        typeInputO.setAttribute('value', 'off');
        typeInputO.setAttribute('class', 'radio');
        typeInputO.setAttribute('data-color', 'lightpink');
        let nameInputO = document.createElement('input');
        nameInputO.setAttribute('min', '3');
        nameInputO.setAttribute('min', '20');
        nameInputO.style.display = 'none';
        petSectionO.appendChild(typeInputO);
        petSectionO.appendChild(typeLabelO);
        petSectionO.appendChild(nameInputO);
        typeInputO.addEventListener("click", (e) => {
            mainSection.style.backgroundColor = e.target.getAttribute("data-color");
            nameInputO.style.display = "block";
            nameInputO.required = true;
        })

        let typeLabelC = document.createElement('label');
        typeLabelC.setAttribute('for', '');
        typeLabelC.textContent = 'Cat';
        let typeInputC = document.createElement('input');
        typeInputC.setAttribute('type', 'radio');
        typeInputC.setAttribute('name', i);
        typeInputC.setAttribute('value', 'off');
        typeInputC.setAttribute('class', 'radio');
        typeInputC.setAttribute('data-color', 'lightgreen');
        petSectionC.appendChild(typeInputC);
        petSectionC.appendChild(typeLabelC);
        typeInputC.addEventListener("click", (e) => {
            mainSection.style.backgroundColor = e.target.getAttribute("data-color");
            nameInputO.style.display = 'none';
            nameInputO.required = false;
        })

        
        let typeLabelD = document.createElement('label');
        typeLabelD.setAttribute('for', '');
        typeLabelD.textContent = 'Dog';
        let typeInputD = document.createElement('input');
        typeInputD.setAttribute('type', 'radio');
        typeInputD.setAttribute('name', i);
        typeInputD.setAttribute('value', 'off');
        typeInputD.setAttribute('class', 'radio');
        typeInputD.setAttribute('data-color', 'lightyellow');
        petSectionD.appendChild(typeInputD);
        petSectionD.appendChild(typeLabelD);
        typeInputD.addEventListener("click", (e) => {
            mainSection.style.backgroundColor = e.target.getAttribute("data-color");
            nameInputO.style.display = 'none';
            nameInputO.required = false;
        })


        

        petSection.appendChild(petSectionC);
        petSection.appendChild(petSectionD);
        petSection.appendChild(petSectionO);

        mainSection.appendChild(nameLabel);
        mainSection.appendChild(nameInput);
        mainSection.appendChild(ageLabel);
        mainSection.appendChild(ageInput);
        mainSection.appendChild(petSection);

        petfield.appendChild(mainSection);

        petsection.style.display = 'inline';
    }

}

select.addEventListener('change', function(e)
{
    let num = Number(e.target.value);
    appendPet(num);
});

//------ code type of pet event registration and event handling
// handler: change the background of the form accordingly



//------ code other type of pet event registration and event handling
//handler: toggles the other pet text section if pet is not a cat or dog



//--------- code pet name duplicate event registration and event handling
//handler: check the name of the pet against the other pet names.
//This uses function validateUniquePetName



//--------- code number-of-pets status event reg and event handling
// handler: 
// a. Dynamic HTML - when number of pets value changes, create a dynamic Array
//	  based on the number create cards where you append newly created inputs
//	  for name, age, and pet type. Inputs are created using a utility function
//	  called createHTMLElem that you will code in utility.js
// b. for each input name, register an event where the handler uses function
//	  validateUniquePetName 

// ------------ validate new pet name is unique

// ----------------------
/**
 * @function - validateUniquePetName
 * @param {String} newPetName
 * @param {Array} petNames 
 * @returns true or false
 * 
 * if newPetName is in petNames then section error is updated with error message
 * No two pets can have the same name
 */
//----------------------------------------