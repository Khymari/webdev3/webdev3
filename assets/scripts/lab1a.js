const btn = document.querySelector('#off');
let state = 0;

function machine_button()
{
  if(state == 0)
  {
    btn.textContent = "Machine is on";
    state = 1;
  } else {
    btn.textContent = "Machine is off";
    state = 0;
  }
}

btn.addEventListener("click", machine_button);