const buttonBar = document.querySelector('#button-bar');
const red = buttonBar.children[0];
const yellow = buttonBar.children[1];
const green = buttonBar.children[2];
const purple = buttonBar.children[3];

function colorChange(event)
{
  buttonBar.style.backgroundColor = event.target.getAttribute("data-color");
}

buttonBar.addEventListener("click", colorChange);