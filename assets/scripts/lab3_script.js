const uname = document.getElementById('uname');

function checkCapital(e)
{
  let x = e.target.value
  let format = RegExp('^[A-Z]');
  if(!format.test(x.value))
  {
    uname.setCustomValidity("Must start with uppercase letter");
  } else {
    uname.setCustomValidity("");
  }
}

// Write it all in your DOMContentLoaded event.
document.addEventListener("DOMContentLoaded", () => {
  uname.addEventListener("blur", checkCapital);
});