const main = document.getElementById("main");

function updateGridView()
{
    if(window.innerWidth < 800)
    {
        main.style.gridTemplateColumns = "repeat(1, 50vw)";
        main.style.gridAutoFlow = "row";
    } else {
        main.style.gridTemplateColumns = "repeat(3, 33vw)";
        main.style.gridAutoFlow = "none";
    }
}

window.addEventListener("resize", updateGridView);