const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');

function drawCircle(x, y, size) {
  ctx.fillStyle = 'white';
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  ctx.beginPath();
  ctx.fillStyle = 'black';
  ctx.arc(x, y, size, 0, 2 * Math.PI);
  ctx.fill();
}

function moveCircle(event)
{
  let key = event.code;
  if(key == "ArrowUp")
  {
    y -= 5;
  } else if(key == "ArrowDown") {
    y += 5;
  } else if(key == "ArrowLeft") {
    x -= 5;
  } else if(key == "ArrowRight") {
    x += 5;
  } else if(key == "Equal") {
    size += 5;
  } else if(key == "Minus") {
    size -= 5;
  }
  drawCircle(x, y, size);
}

let x = 50;
let y = 50;
let size = 30;

drawCircle(x, y, size);

document.addEventListener("keydown", moveCircle);